<?php

/**
 * @file
 * Handles all things concerning Ubercart marketplace products.
 *
 * The Ubercart marketplace product enhancement allows sellers to view their
 * items and edit them.  This module also includes concept code for manipulating the product edit page to suit the sellers' needs.
 *
 */

/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implementation of hook_perm().
 */
function mp_products_perm() {
  return array('view own products', 'act as seller');
}

/**
 * Implementation of hook_help().
 */
function mp_products_help($section) {
  switch ($section) {
    case 'user/'. arg(1) .'/selling':
      return '<p>'. t("The following is a list of all the products you have submitted. You can edit a product by clicking the edit icon next to its name.") .'</p>';
  }
}

/**
 * Implementation of hook_user().
 */
function mp_products_user($op, &$edit, &$account, $category = NULL) {
  global $user;
  switch ($op) {
    case 'view':
      if (user_access('view own products')) {
        $items['products'] = array(
          'value' => l(t('Click here to view your products.'), 'user/'. $account->uid .'/selling/'),
          'class' => 'member',
        );
      }
      if (module_exists(mp_orders) && user_access('fulfill own orders')) {
        $items['fulfill'] = array(
          'value' => l(t('Click here to fulfill your orders.'), 'user/'. $account->uid .'/selling/fulfill'),
          'class' => 'member',
        );
      }
      if (module_exists(mp_reports) && user_access('view own reports')) {
        $items['reports'] = array(
          'value' => l(t('Click here to view your sales reports.'), 'user/'. $account->uid .'/selling/reports'),
          'class' => 'member',
        );
      }
      if (sizeof($items) != 0) {
          return array(t('Selling') => $items);
      }
      else {
        return NULL;
      }
    break;
  }
}

/**
 * Implementation of hook_menu().
 */
function mp_products_menu($may_cache) {
  global $user;
  $items = array();
  
  if ($user->uid) {
    if (!$may_cache && is_numeric(arg(1))) {
      $items[] = array(
        'path' => 'user/'. arg(1) .'/selling',
        'title' => t('Selling'),
        'description' => t('View and manage your products and orders.'),
        'callback' => 'mp_products_selling',
        'callback arguments' => array(arg(1)),
        'access' => user_access('view own products') && ($user->uid == arg(1) || $user->uid == 1),
        'type' => MENU_LOCAL_TASK,
      );
      $items[] = array(
        'path' => 'user/'. arg(1) .'/selling/view',
        'title' => t('View Products'),
        'description' => t('View and edit your products.'),
        'callback' => 'mp_products_selling',
        'callback arguments' => array(arg(1)),
        'access' => user_access('view own products') && ($user->uid == arg(1) || $user->uid == 1),
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );
    } 
    else {
      $items[] = array(
        'path' => 'admin/store/settings/seller',
        'title' => t('Seller settings'),
        'access' => user_access('administer products'),
        'description' => t('Configure the marketplace seller settings.'),
        'callback' => 'drupal_get_form',
        'callback arguments' => array('mp_products_settings_seller_form'),
        'type' => MENU_NORMAL_ITEM,
      );
     $items[] = array(
        'path' => 'admin/store/settings/seller/commission',
        'title' => t('Commission rate settings'),
        'access' => user_access('administer products'),
        'description' => t('Configure the marketplace seller commission rates.'),
        'callback' => 'drupal_get_form',
        'callback arguments' => array('mp_products_commission_form'),
        'type' => MENU_CALLBACK,
      );
    }
  }
  return $items;
}

/**
 * Implementation of hook_form_alter().
 * 
 * Adds minimum sell price field and adds validate handler.
 */
function mp_products_form_alter($form_id, &$form) {
  $node = $form['#node'];
  if (is_object($node) && $form_id == $node->type .'_node_form' && in_array($node->type, module_invoke_all('product_types'))) {
    $sign_flag = variable_get('uc_sign_after_amount', FALSE);
    $currency_sign = variable_get('uc_currency_sign', '$');

    // Handle adding min price field if enabled
    if (variable_get('mp_products_enable_min', FALSE)) {
      $form['base']['prices']['min_sell_price'] = array(
        '#type' => 'textfield',
        '#title' => t('Minimum sell price'),
        '#required' => TRUE,
        '#default_value' => number_format(floatval($node->min_sell_price), 2, '.', ''),
        '#description' => t('Minimum allowed advertised price for this item.'),
        '#weight' => 4,
        '#size' => 20,
        '#maxlength' => 35,
        '#field_prefix' => $sign_flag ? '' : $currency_sign,
        '#field_suffix' => $sign_flag ? $currency_sign : '',
      );
    }

    // Modify cost field depending on auto calc commission status
    $form['base']['prices']['cost']['#title'] = t('Seller commission');
    if (!variable_get('mp_products_auto_calc', FALSE)) {
      $form['base']['prices']['cost']['#required'] = TRUE;
      $form['base']['prices']['cost']['#description'] = t('The amount you will be paid.');
      if (!variable_get('mp_products_enable_list', TRUE) && !user_access('administer products')) {
        $form['base']['prices']['list_price']['#access'] = FALSE;
      }
    }
    else {
      $form['base']['prices']['cost']['#access'] = user_access('administer products');
      if(!variable_get('mp_products_admin_override', FALSE)) {
        $form['base']['prices']['cost']['#attributes'] = array('readonly' => 'readonly');
      }
      $form['base']['prices']['cost']['#description'] = t('The amount seller will be paid.');
      // determine if list_price should be required
      if (variable_get('mp_products_auto_calc_field', 'sell_price') == 'list_price') {
        $form['base']['prices']['list_price']['#required'] = TRUE;
      }
      else if (!variable_get('mp_products_enable_list', TRUE) && !user_access('administer products')) {
        $form['base']['prices']['list_price']['#access'] = FALSE;
      }
    }

    // Add Base Price amount to Sell Price description, if there is a base price.
    if (variable_get('mp_products_admin_base', FALSE)) {
    	$form['base']['prices']['sell_price']['#description'] .= t(' (Must be at least '.uc_currency_format(variable_get('mp_products_admin_base', FALSE)).')');
    }

    // hide unneccessary fields if desired
    if (!user_access('administer products') && variable_get('mp_products_hide_fields', TRUE)) {
      $form['base']['weight']['#access'] = FALSE;
      $form['base']['dimensions']['#access'] = FALSE;
      $form['base']['pkg_qty']['#access'] = FALSE;
      $form['base']['default_qty']['#access'] = FALSE;
      $form['base']['ordering']['#access'] = FALSE;
      $form['shipping']['#access'] = FALSE;
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function mp_products_nodeapi(&$node, $op, $a3 = NULL) {
  if (in_array($node->type, module_invoke_all('product_types'))) {
    switch ($op) {
      case 'insert':
      case 'update':
        // handle min_sell_price insertion if enabled
        if (isset($node->min_sell_price)) {
          $min_sell_price_value = $node->min_sell_price;
          if (!$node->revision) {
            db_query("DELETE FROM {mp_seller_min_sell_prices} WHERE vid = %d", $node->vid);
          }
          db_query("INSERT INTO {mp_seller_min_sell_prices} (nid, vid, min_sell_price) VALUES (%d, %d, '%d')",
            $node->nid, $node->vid, $min_sell_price_value);
        }
      break;
      case 'submit':
        $commission_rate = mp_products_get_commission_rate($node->type);
        // auto calc commission if enabled and needed
        if (variable_get('mp_products_auto_calc', FALSE) && !(user_access('administer products') && variable_get('mp_products_admin_override', FALSE))) {
          if (variable_get('mp_products_auto_calc_field', 'sell_price') == 'list_price') {
            $node->cost = $node->list_price * $commission_rate;
          }
          else {
            $node->cost = $node->sell_price * $commission_rate;
          }
        }
        // invoke hook_list_product()
        module_invoke_all('list_product', &$node);
      break;
      case 'prepare':
        if (!variable_get('mp_products_auto_calc', FALSE) && variable_get('mp_products_insert_js', FALSE)) {
          $commission_rate = mp_products_get_commission_rate($node->type);
          $fieldid = variable_get('mp_products_auto_calc_field', 'sell_price') == 'list_price' ? 'list-price' : 'sell-price';
          drupal_add_js("$(document).ready(function(){ $('#edit-". $fieldid ."').blur(function(){ $('#edit-cost').val(Math.floor($('#edit-". $fieldid ."').val() * ". drupal_to_js($commission_rate)* 100 .")/100);});});", 'inline');
        }
      break;
      case 'load':
        if (variable_get('mp_products_enable_min', FALSE)) {
          return array('min_sell_price' => db_result(db_query("SELECT min_sell_price FROM {mp_seller_min_sell_prices} WHERE vid = %d", $node->vid)));
        }
      break;
      case 'validate':

        // handle cost field validation depending on auto calc commission or admin status
        if (!variable_get('mp_products_auto_calc', FALSE) && !(user_access('administer products') && variable_get('mp_products_admin_override', FALSE))) {
          $commission_rate = mp_products_get_commission_rate($node->type);
          if ($node->cost > ($commission_rate * $node->sell_price)) {
            form_set_error('cost', t('Commission must be less than or equal to @rate% of the sell price. Consider decreasing commission or increasing sell price.', array('@rate' => $commission_rate*100)));
          }
          else if ($node->cost == 0 && !variable_get('mp_products_auto_calc', FALSE)) {
            form_set_error('cost', t('Commission must be greater than zero.'));
          }
        }
        // handle min_sell_price validation if enabled
        if (variable_get('mp_products_enable_min', FALSE)) {
          $pattern = '/^\d*(\.\d*)?$/';
          $price_error = t('Minimum sell price must be in a valid number format. No commas and only one decimal point.');
          if (!is_numeric($node->min_sell_price) && !preg_match($pattern, $node->min_sell_price)) {
            form_set_error('min_sell_price', $price_error);
          }
          else if ($node->min_sell_price > $node->sell_price) {
            form_set_error('min_sell_price', t('Minimum sell price must be less than or equal to sell price.'));
          }
        }
        
        // handle base price error, if a base price exists
        if (variable_get('mp_products_admin_base', FALSE)) {
          if ($node->sell_price < variable_get('mp_products_admin_base', FALSE)) {            
            form_set_error('sell_price', t('Sell price must be greater than or equal to sitewide base price of '.uc_currency_format(variable_get('mp_products_admin_base', FALSE))));
          }
        }
      break;
      case 'delete':
          db_query("DELETE FROM {mp_seller_min_sell_prices} WHERE nid = %d", $node->nid);
      break;
      case 'delete revision':
          db_query("DELETE FROM {mp_seller_min_sell_prices} WHERE vid = %d", $node->vid);
      break;
    }
  }
}

function mp_products_get_commission_rate($type) {
  global $user;
  $user_roles = array_keys($user->roles);
  $roles_array = array_keys(user_roles(true, 'act as seller'));
  $rid = -1;
  foreach ($user_roles as $role) {
    if (in_array($role, $roles_array)) {
      $rid = $role;
    }
  }
  $result = db_result(db_query("SELECT rate FROM {mp_seller_rates} WHERE class = '%s' AND rid = %d", $type, $rid));
  if ($result > 0) {
    return floatval($result);
  }
  else {
    return floatval(variable_get('mp_products_commission_rate', '.75'));
  }
}

/**
 * theme_uc_product_form_price override to include min_sell_price
 */
function phptemplate_uc_product_form_prices($prices) {
  return '<table><tr><td>'."\n". drupal_render($prices['list_price'])
    .'</td><td>'. drupal_render($prices['cost'])
    .'</td><td>'. drupal_render($prices['sell_price'])
    .'</td><td>'. drupal_render($prices['min_sell_price'])
    ."</td></tr></table>\n";
}

/**
 * Seller settings form 
 */
function mp_products_settings_seller_form() {
  $form['mp_product_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Product form settings'
  );
  $form['mp_product_fieldset']['mp_products_hide_fields'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide extra fields'),
      '#description' => t('Check to hide product node fields unnecessary for marketplace sellers.'),
      '#default_value' => variable_get('mp_products_hide_fields', TRUE),
  );
  $form['mp_product_fieldset']['mp_products_enable_min'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show min sell price'),
      '#description' => t('Check to show minimum sell price field on product form.  Useful as an instruction to tell store administrator what the lowest price the seller will allow his/her product to be advertised for.'),
      '#default_value' => variable_get('mp_products_enable_min', FALSE),
  );
  $form['mp_product_fieldset']['mp_products_enable_list'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show list price'),
      '#description' => t('Check to show list price field on product form.  DO NOT uncheck if autofill or autocalc is turned on below AND commission field is LIST PRICE.'),
      '#default_value' => variable_get('mp_products_enable_list', TRUE),
  );
  $form['mp_product_fieldset']['mp_products_admin_base'] = array(
      '#type' => 'textfield',
      '#title' => t('Administrator base price'),
      '#description' => t('Here you can type the bare minimum a seller must set their price point to. (Useful for a "base price" for sellers to go from).'),
      '#default_value' => variable_get('mp_products_admin_base', FALSE),
  );
  $form['mp_product_fieldset']['commission_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Commission calculation settings',
  );
  $form['mp_product_fieldset']['commission_fieldset']['mp_products_commission_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Commission Rate'),
      '#description' => t('Enter decimal fraction of sell price or list price to be used as seller\'s commission rate.  This is the fraction of the sell price seller will be paid for a product sale. ') . l(t('Configure commission rates by seller role and product class.'), 'admin/store/settings/seller/commission'),
      '#default_value' => variable_get('mp_products_commission_rate', '.75'),
    '#size' => 10,
  );
  $form['mp_product_fieldset']['commission_fieldset']['mp_products_auto_calc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatically calculate commission'),
      '#description' => t('Check to automatically calculate amount (commission) seller will be paid for a sale of a product. Value calculated is based on commission rate. Checking this hides seller commission field. NOTE: Changing this does not refresh existing products.'),
      '#default_value' => variable_get('mp_products_auto_calc', FALSE),
  );
  $form['mp_product_fieldset']['commission_fieldset']['mp_products_auto_calc_field'] = array(
      '#type' => 'select',
      '#title' => t('Commission based on'),
      '#description' => t('Select the field the commission a seller will be paid is based on. ONLY applies if commission auto calc or autofill is turned ON. NOTE: Changing this does not refresh existing products.'),
      '#default_value' => variable_get('mp_products_auto_calc_field', 'sell_price'),
      '#options' => array(
        'sell_price' => t('Sell Price'),
        'list_price' => t('List Price'),
      ),
  );
  $form['mp_product_fieldset']['commission_fieldset']['mp_products_admin_override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow admin commission override'),
      '#description' => t('Check to ignore automatically calculated commission in favor of administrator specified commission value when entered. Additionally, checking this removes commission field validation for admin.'),
      '#default_value' => variable_get('mp_products_admin_override', FALSE),
  );
  $form['mp_product_fieldset']['commission_fieldset']['mp_products_insert_js'] = array(
      '#type' => 'checkbox',
      '#title' => t('Autofill commission field'),
      '#description' => t('Check to insert javascript autofill function for seller commission field. ONLY applies if commission auto calc is turned OFF.'),
      '#default_value' => variable_get('mp_products_insert_js', FALSE),
  );
  if (module_exists(mp_file)) {
    $form['mp_file_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => 'File download feature settings'
    );
    $form['mp_file_fieldset']['mp_file_field_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name of product filefield'),
      '#default_value' => variable_get('mp_file_field_name', 'download'),
      '#description' => t('Enter the machine-readable name of the filefield field with which sellers will upload files.'),
      '#size' => 100,
    );
    $form['mp_file_fieldset']['mp_file_allow_sellers_perm'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow sellers to commit files'),
      '#description' => t('Check to have files automatically committed to a file download feature.  Useful if products are not moderated. WARNING: Enabling this has a potential security risk.'),
      '#default_value' => variable_get('mp_file_allow_sellers_perm', FALSE),
    );
  }
  if (module_exists(mp_reports)) {
    $form['mp_reports_payment'] = array(
      '#type' => 'fieldset',
      '#title' => 'Seller payment options'
    );
    $form['mp_reports_payment']['mp_reports_display_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add Paypal email address field'),
      '#description' => t('Check to add an email address field to seller profile edit forms.  This email address will be the one used for Paypal Masspay functionality.  IMPORTANT: To allow admin to actually send payments via Paypal, uc_paypal must be enabled and api credentials must be entered for WPP (even if not enabled as a gateway).'),
      '#default_value' => variable_get('mp_reports_display_email', TRUE),
    );
    $form['mp_reports_payment']['mp_reports_enable_check'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable manual payment method'),
      '#description' => t('Check to enable the manual seller payment method (currently: check).  Enabling this will add a payment details field to seller profile edit forms.'),
      '#default_value' => variable_get('mp_reports_enable_check', FALSE),
    );
  }
  return system_settings_form($form);
}

function mp_products_commission_form() {
  $roles_array = user_roles(true, 'act as seller');
  $classes_array = module_invoke_all('product_types');
  $form = array();
  $form['#tree'] = TRUE;
  foreach ($classes_array as $class) {
    $form[$class]['title'] = array('#value' => $class);
    foreach ($roles_array as $role) {
      $form[$class][$role] = array(
        '#type' => 'textfield',
        '#description' => $role,
        '#size' => 10,
        '#default_value' => db_result(db_query("SELECT rate FROM {mp_seller_rates} WHERE rid = %d AND class = '%s'", array_search($role, $roles_array), $class)),
      );
    }
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  return $form;
}

function mp_products_commission_form_submit($form_id, $form_values) {
  $classes_array = module_invoke_all('product_types');
  $roles_array = user_roles(true, 'act as seller');
  foreach ($classes_array as $class) {
    foreach ($roles_array as $role) {
      db_query("UPDATE {mp_seller_rates} SET rate = %f WHERE rid = %d AND class = '%s'", $form_values[$class][$role], array_search($role, $roles_array), $class);
      if (!db_affected_rows()) {
        db_query("INSERT INTO {mp_seller_rates} (rate, rid, class) VALUES (%f, %d, '%s')", $form_values[$class][$role], array_search($role, $roles_array), $class);
      }
    }
  }
  drupal_set_message(t('Rates saved.'));
}

/**
* Returns a sortable table listing of the seller's products.
*
* @param $uid
*   The user ID whose marketplace products to show.
*/
function mp_products_selling($uid) {
  drupal_set_title(t('My Products'));

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('My account'), 'user/'. arg(1));
  $breadcrumb[] = l(t('Selling'), 'user/'. arg(1) .'/selling');
  drupal_set_breadcrumb($breadcrumb);

  $header = array(
    array('data' => t('ID'), 'field' => 'n.nid'),
    array('data' => t('Name'), 'field' => 'n.title', 'sort' => 'desc'),
    array('data' => t('MSRP'), 'field' => 'p.list_price'),
    array('data' => t('Cost'), 'field' => 'p.cost'),
    array('data' => t('Added'), 'field' => 'n.created'),
    array('data' => t('Status'), 'field' => 'n.status')
  );
  $result = pager_query("SELECT n.nid, n.title, p.list_price, p.cost, n.created, n.status FROM {node} AS n INNER JOIN {uc_products} AS p USING (nid) WHERE n.uid = %d"  . tablesort_sql($header), 20, 0, "SELECT COUNT(*) FROM {node} WHERE uid = %d", $uid);

  // Build a table based on the seller's products.
  while ($product = db_fetch_object($result)) {
    $link = l($product->title, 'node/'. $product->nid);
    $link .= '<span class="order-admin-icons">'. l('<img src="'. base_path() . drupal_get_path('module', 'uc_store') .'/images/order_view.gif" alt="View Product" />', 'node/'. $product->nid, array('title' => 'View Product'), NULL, NULL, NULL, TRUE) . l('<img src="'. base_path() . drupal_get_path('module', 'uc_store') .'/images/order_edit.gif" alt="Edit Product" />', 'node/'. $product->nid .'/edit', array('title' => 'Edit Product'), NULL, NULL, NULL, TRUE) .'</span>';
    $listed = ($product->status == 0) ? 'Not Currently Listed' : 'For Sale';

    $rows[] = array(
      array('data' => $product->nid),
      array('data' => $link),
      array('data' => uc_currency_format($product->list_price, TRUE), 'align' => 'left'),
      array('data' => uc_currency_format($product->cost, TRUE), 'align' => 'left'),
        array('data' => format_date($product->created, 'custom', variable_get('uc_date_format_default', 'm/d/Y'))),
      array('data' => $listed)
    );
  }
  $output = theme('table', $header, $rows) . theme('pager', null, 20, 0);
  
  return $output;
}